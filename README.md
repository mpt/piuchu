# Getting Started

First, get the code:

```
$ git clone https://git.framasoft.org/mpt/piuchu.git
$ cd piuchu
```

Next, create a Python virtual environment (we recommend using
[Pew](https://pypi.python.org/pypi/pew)):

```
$ pew new -p python3 -a $(pwd) piuchu
```

Next, install all the dependencies, and setup the application for development:

```
$ pip install -r requirements.txt
$ python setup.py develop
```

At this point, it is probably a good idea to run the unit tests:

```
$ pip install -r requirements-dev.txt
$ py.test
```

If everything succeeded, you can now create the database model, then run the
application:

```
$ piuchu-initdb development.ini
$ pserve development.ini
```
