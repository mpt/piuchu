# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of Piuchu
#
# Piuchu is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Piuchu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Piuchu.  If not, see <http://www.gnu.org/licenses/>.


import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()
with open(os.path.join(here, 'requirements.txt')) as f:
    REQUIRES = []
    for line in f:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        REQUIRES.append(line)
with open(os.path.join(here, 'requirements-dev.txt')) as f:
    DEV_REQUIRES = []
    for line in f:
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        DEV_REQUIRES.append(line)


setup(
    name='piuchu',
    version='0.0',
    description='piuchu',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Pyramid',
        ('License :: OSI Approved :: GNU Affero General Public License v3 or '
         'later (AGPLv3+)'),
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        ],
    author='See the AUTHORS file',
    author_email='See the AUTHORS file',
    url='https://git.framasoft.org/mpt/piuchu',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='piuchu',
    install_requires=REQUIRES,
    tests_require=DEV_REQUIRES,
    entry_points={
        'paste.app_factory': [
            'main=piuchu:main',
            ],
        'console_scripts': [
            'piuchu-initdb=piuchu.scripts.initializedb:main',
            ],
        },
    )
