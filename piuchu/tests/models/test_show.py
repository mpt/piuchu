# Copyright (c) 2016 - Anaïs Vidal <anais.vidal@openmailbox.org>
# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of Piuchu
#
# Piuchu is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Piuchu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Piuchu.  If not, see <http://www.gnu.org/licenses/>.


import transaction


def test_new_show_type(db_session):
    from piuchu.models import ShowType

    with transaction.manager:
        db_session.add(ShowType(name='Jeune Public'))

    showtypes = db_session.query(ShowType)
    assert showtypes.count() == 1

    st = showtypes.one()
    assert st.name == 'Jeune Public'


def test_new_show(db_session):
    from piuchu.models import Show, ShowType

    with transaction.manager:
        st = ShowType(name='Jeune Public')
        db_session.add(st)

        db_session.add(Show(name='Petit Pouette', type=st))

    shows = db_session.query(Show)
    assert shows.count() == 1

    s = shows.one()
    assert s.name == 'Petit Pouette'
    assert s.type.name == 'Jeune Public'
