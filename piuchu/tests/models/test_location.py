# Copyright (c) 2016 - Anaïs Vidal <anais.vidal@openmailbox.org>

# This file is part of Piuchu
#
# Piuchu is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Piuchu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Piuchu.  If not, see <http://www.gnu.org/licenses/>.


import pytest
from sqlalchemy.exc import IntegrityError
import transaction


def test_new_location(db_session):
    from piuchu.models import Location

    with transaction.manager:
        db_session.add(Location(name="Salle des rancy"))

    locations = db_session.query(Location)
    assert locations.count() == 1

    l = locations.one()
    assert l.name == "Salle des rancy"


def test_location_name_is_required(db_session):
    from piuchu.models import Location

    with pytest.raises(IntegrityError):
        with transaction.manager:
            db_session.add(Location())
