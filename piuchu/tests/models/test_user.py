# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of Piuchu
#
# Piuchu is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Piuchu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Piuchu.  If not, see <http://www.gnu.org/licenses/>.


import pytest
import transaction


def test_new_user(db_session):
    from piuchu.models import User

    with transaction.manager:
        db_session.add(User(login='foo', password='foo', fullname='Foo Bar', email='foo@bar.com'))

    users = db_session.query(User)
    assert users.count() == 1

    u = users.one()
    assert u.login == 'foo'
    assert u.password != 'foo'
    assert u.verify_password('foo')
    assert u.fullname == 'Foo Bar'
    assert u.email == 'foo@bar.com'
    assert u.phone is None
    assert u.mobile is None


def test_at_least_email_or_phone_or_mobile(db_session):
    from piuchu.models import User

    with pytest.raises(ValueError):
        with transaction.manager:
           db_session.add(User(login='foo', password='foo', fullname='Foo Bar'))
