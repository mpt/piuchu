# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of Piuchu
#
# Piuchu is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Piuchu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Piuchu.  If not, see <http://www.gnu.org/licenses/>.


import pyramid.testing
import pytest


@pytest.yield_fixture
def app_config():
    settings = {'sqlalchemy.url': 'sqlite:///:memory:'}
    config = pyramid.testing.setUp(settings=settings)
    config.include('piuchu.models')

    yield config

    pyramid.testing.tearDown()


@pytest.fixture
def db_session(app_config):
    from piuchu.models import Base

    session = app_config.registry['db_sessionmaker']()
    engine = session.bind
    Base.metadata.create_all(engine)

    return session
