# Copyright (c) 2016 - Anaïs Vidal <anais.vidal@openmailbox.org>
# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of Piuchu
#
# Piuchu is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Piuchu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Piuchu.  If not, see <http://www.gnu.org/licenses/>.


from bcrypt import gensalt, hashpw

from sqlalchemy.event import listens_for
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Integer, LargeBinary, Unicode
from sqlalchemy.orm import relationship


Base = declarative_base()


PASSWD_WORK_FACTOR = 12


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    login = Column(Unicode, unique=True, nullable=False)
    _password = Column('password', LargeBinary, nullable=False)
    fullname = Column(Unicode, nullable=False)
    email = Column(Unicode)
    phone = Column(Unicode)
    mobile = Column(Unicode)

    # -- Properties and Methods ------------------------------------
    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        password = password.encode('utf-8')
        self._password = hashpw(password, gensalt(rounds=PASSWD_WORK_FACTOR))

    def verify_password(self, password):
        password = password.encode('utf-8')
        return hashpw(password, self._password) == self._password


class ShowType(Base):
    __tablename__ = 'show_types'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False)


class Show(Base):
    __tablename__ = 'shows'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False)
    type_id = Column(Integer, ForeignKey("show_types.id"), nullable=False)

    # -- Relationships ---------------------------------------------
    type = relationship("ShowType")


class Location(Base):
    __tablename__ = 'locations'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode, nullable=False)


@listens_for(User, 'before_insert')
@listens_for(User, 'before_update')
def before_save_user(mapper, connection, user):
    """Acts when saving a user"""
    if user.email is None and user.phone is None and user.mobile is None:
        # TODO: Use the proper exception type
        raise ValueError(
            'A user must have at least an email, phone or mobile')


def includeme(config):
    from sqlalchemy.engine import engine_from_config
    from sqlalchemy.orm import sessionmaker

    from zope.sqlalchemy import register

    settings = config.get_settings()
    engine = engine_from_config(settings, 'sqlalchemy.')
    maker = sessionmaker()
    register(maker)
    maker.configure(bind=engine)

    config.registry['db_sessionmaker'] = maker
